#include <systemc.h>
#include <iostream>

SC_MODULE(Hello) {
	SC_CTOR(Hello) {
		SC_THREAD(salutation);
	}

	void salutation() {
		std::cout << "Hello SystemC world!" << std::endl;
	}
};

int sc_main(int sc_argc, char* sc_argv[]) {
	Hello hello("helloInstance");
	sc_start();
	return 0;
}

